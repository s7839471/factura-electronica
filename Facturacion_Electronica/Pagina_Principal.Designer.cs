﻿
namespace Facturacion_Electronica
{
    partial class Pagina_Principal
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Pagina_Principal));
            this.btn_CFBuscarDocumento = new System.Windows.Forms.Button();
            this.Lbl_Abrir = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tp_ParametrosGenerales = new System.Windows.Forms.TabPage();
            this.btn_PGVolver = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.btn_PGActualizar = new System.Windows.Forms.Button();
            this.btn_PGAgregar = new System.Windows.Forms.Button();
            this.btn_PGConsulta = new System.Windows.Forms.Button();
            this.cb_TipoComp = new System.Windows.Forms.ComboBox();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.btn_PGGuardar = new System.Windows.Forms.Button();
            this.dgv_ParametrosGenerales = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoComprobante = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Empresa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_id = new System.Windows.Forms.TextBox();
            this.tp_ParametrosContables = new System.Windows.Forms.TabPage();
            this.lbl_IdCliente = new System.Windows.Forms.Label();
            this.btn_PCEditarCliente = new System.Windows.Forms.Button();
            this.btn_PCNombreCliente = new System.Windows.Forms.ComboBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.lbl_PCNombreCliente = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cb_PCTipoID = new System.Windows.Forms.ComboBox();
            this.dgv_ParametrosContables = new System.Windows.Forms.DataGridView();
            this.tipoIdCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NoIdCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.btn_PCAgregarCliente = new System.Windows.Forms.Button();
            this.btn_PCBuscarCliente = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.cb_PCContabilidad = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.tp_CargarFactura = new System.Windows.Forms.TabPage();
            this.cb_CFTipoIdCliente = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.tb_CFNoIdClente = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.tb_CFFechaEmision = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tb_CFNoFactura = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cb_CFTipoId = new System.Windows.Forms.ComboBox();
            this.cb_CFContabiliad = new System.Windows.Forms.ComboBox();
            this.tb_CFNoId = new System.Windows.Forms.TextBox();
            this.dgv_FacturaCargada = new System.Windows.Forms.DataGridView();
            this.TipoId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Desription = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label18 = new System.Windows.Forms.Label();
            this.tp_FacturasCargadas = new System.Windows.Forms.TabPage();
            this.label21 = new System.Windows.Forms.Label();
            this.btn_FCGenerarComprobante = new System.Windows.Forms.Button();
            this.btn_FCGuardarExcel = new System.Windows.Forms.Button();
            this.btn_FCFiltro = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.cb_FCEmpresaFactura = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cb_FCEstado = new System.Windows.Forms.ComboBox();
            this.btn_FCVolver = new System.Windows.Forms.Button();
            this.dgv_FCFacturasCargadas = new System.Windows.Forms.DataGridView();
            this.FC_Grid_Contabilidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FC_Grid_NoIdEmpFactura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FC_Grid_NoIdCliente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FC_Grid_FechaEmision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FC_Grid_VContable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FC_Grid_Valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FC_Grid_TMov = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.label12 = new System.Windows.Forms.Label();
            this.tb_FCIdFactura = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FC_Grid_Estado = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btn_FCConsultaFactura = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.Contabilidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btn_MenuParametrosGenerales = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_MenuSalir = new System.Windows.Forms.Button();
            this.btn_MenuFacturasCargadas = new System.Windows.Forms.Button();
            this.btn_MenuCargarFactura = new System.Windows.Forms.Button();
            this.btn_MenuParametrosContables = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tp_ParametrosGenerales.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ParametrosGenerales)).BeginInit();
            this.tp_ParametrosContables.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ParametrosContables)).BeginInit();
            this.tp_CargarFactura.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_FacturaCargada)).BeginInit();
            this.tp_FacturasCargadas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_FCFacturasCargadas)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_CFBuscarDocumento
            // 
            this.btn_CFBuscarDocumento.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btn_CFBuscarDocumento.FlatAppearance.BorderSize = 0;
            this.btn_CFBuscarDocumento.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_CFBuscarDocumento.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btn_CFBuscarDocumento.Location = new System.Drawing.Point(626, 18);
            this.btn_CFBuscarDocumento.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_CFBuscarDocumento.Name = "btn_CFBuscarDocumento";
            this.btn_CFBuscarDocumento.Size = new System.Drawing.Size(155, 30);
            this.btn_CFBuscarDocumento.TabIndex = 0;
            this.btn_CFBuscarDocumento.Text = "Buscar Documento";
            this.btn_CFBuscarDocumento.UseVisualStyleBackColor = false;
            this.btn_CFBuscarDocumento.Click += new System.EventHandler(this.btn_CFBuscarDocumento_Click);
            // 
            // Lbl_Abrir
            // 
            this.Lbl_Abrir.AutoSize = true;
            this.Lbl_Abrir.Font = new System.Drawing.Font("Segoe UI", 9.75F);
            this.Lbl_Abrir.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Lbl_Abrir.Location = new System.Drawing.Point(11, 152);
            this.Lbl_Abrir.Name = "Lbl_Abrir";
            this.Lbl_Abrir.Size = new System.Drawing.Size(0, 17);
            this.Lbl_Abrir.TabIndex = 1;
            this.Lbl_Abrir.Visible = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(25, 207);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 18);
            this.label2.TabIndex = 4;
            this.label2.Text = "No Id:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(15, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 18);
            this.label3.TabIndex = 5;
            this.label3.Text = "Contabilidad :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(22, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "Tipo id:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tp_ParametrosGenerales);
            this.tabControl1.Controls.Add(this.tp_ParametrosContables);
            this.tabControl1.Controls.Add(this.tp_CargarFactura);
            this.tabControl1.Controls.Add(this.tp_FacturasCargadas);
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.tabControl1.Location = new System.Drawing.Point(212, -23);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(829, 637);
            this.tabControl1.TabIndex = 11;
            // 
            // tp_ParametrosGenerales
            // 
            this.tp_ParametrosGenerales.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.tp_ParametrosGenerales.Controls.Add(this.btn_PGVolver);
            this.tp_ParametrosGenerales.Controls.Add(this.button8);
            this.tp_ParametrosGenerales.Controls.Add(this.btn_PGActualizar);
            this.tp_ParametrosGenerales.Controls.Add(this.btn_PGAgregar);
            this.tp_ParametrosGenerales.Controls.Add(this.btn_PGConsulta);
            this.tp_ParametrosGenerales.Controls.Add(this.cb_TipoComp);
            this.tp_ParametrosGenerales.Controls.Add(this.comboBox9);
            this.tp_ParametrosGenerales.Controls.Add(this.btn_PGGuardar);
            this.tp_ParametrosGenerales.Controls.Add(this.dgv_ParametrosGenerales);
            this.tp_ParametrosGenerales.Controls.Add(this.comboBox8);
            this.tp_ParametrosGenerales.Controls.Add(this.label7);
            this.tp_ParametrosGenerales.Controls.Add(this.label6);
            this.tp_ParametrosGenerales.Controls.Add(this.label5);
            this.tp_ParametrosGenerales.Controls.Add(this.txt_id);
            this.tp_ParametrosGenerales.Controls.Add(this.label2);
            this.tp_ParametrosGenerales.Controls.Add(this.label3);
            this.tp_ParametrosGenerales.Controls.Add(this.label4);
            this.tp_ParametrosGenerales.Location = new System.Drawing.Point(4, 25);
            this.tp_ParametrosGenerales.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tp_ParametrosGenerales.Name = "tp_ParametrosGenerales";
            this.tp_ParametrosGenerales.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tp_ParametrosGenerales.Size = new System.Drawing.Size(821, 608);
            this.tp_ParametrosGenerales.TabIndex = 0;
            this.tp_ParametrosGenerales.Text = "Parametros Generales";
            // 
            // btn_PGVolver
            // 
            this.btn_PGVolver.BackColor = System.Drawing.Color.DarkGray;
            this.btn_PGVolver.FlatAppearance.BorderSize = 0;
            this.btn_PGVolver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_PGVolver.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.btn_PGVolver.Location = new System.Drawing.Point(466, 206);
            this.btn_PGVolver.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_PGVolver.Name = "btn_PGVolver";
            this.btn_PGVolver.Size = new System.Drawing.Size(76, 34);
            this.btn_PGVolver.TabIndex = 114;
            this.btn_PGVolver.Text = "Volver";
            this.btn_PGVolver.UseVisualStyleBackColor = false;
            this.btn_PGVolver.Visible = false;
            this.btn_PGVolver.Click += new System.EventHandler(this.btn_PGVolver_Click);
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.button8.Location = new System.Drawing.Point(466, 206);
            this.button8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(76, 34);
            this.button8.TabIndex = 113;
            this.button8.Text = "Volver";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Visible = false;
            this.button8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // btn_PGActualizar
            // 
            this.btn_PGActualizar.BackColor = System.Drawing.Color.DarkGray;
            this.btn_PGActualizar.FlatAppearance.BorderSize = 0;
            this.btn_PGActualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_PGActualizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.btn_PGActualizar.Location = new System.Drawing.Point(311, 172);
            this.btn_PGActualizar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_PGActualizar.Name = "btn_PGActualizar";
            this.btn_PGActualizar.Size = new System.Drawing.Size(149, 34);
            this.btn_PGActualizar.TabIndex = 112;
            this.btn_PGActualizar.Text = "Guardar";
            this.btn_PGActualizar.UseVisualStyleBackColor = false;
            this.btn_PGActualizar.Visible = false;
            this.btn_PGActualizar.Click += new System.EventHandler(this.btn_PGActualizar_Click);
            // 
            // btn_PGAgregar
            // 
            this.btn_PGAgregar.BackColor = System.Drawing.Color.DarkGray;
            this.btn_PGAgregar.FlatAppearance.BorderSize = 0;
            this.btn_PGAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_PGAgregar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.btn_PGAgregar.Location = new System.Drawing.Point(642, 162);
            this.btn_PGAgregar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_PGAgregar.Name = "btn_PGAgregar";
            this.btn_PGAgregar.Size = new System.Drawing.Size(149, 34);
            this.btn_PGAgregar.TabIndex = 111;
            this.btn_PGAgregar.Text = "Agregar";
            this.btn_PGAgregar.UseVisualStyleBackColor = false;
            this.btn_PGAgregar.Click += new System.EventHandler(this.btn_PGAgregar_Click);
            // 
            // btn_PGConsulta
            // 
            this.btn_PGConsulta.BackColor = System.Drawing.Color.DarkGray;
            this.btn_PGConsulta.FlatAppearance.BorderSize = 0;
            this.btn_PGConsulta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_PGConsulta.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.btn_PGConsulta.Location = new System.Drawing.Point(642, 207);
            this.btn_PGConsulta.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_PGConsulta.Name = "btn_PGConsulta";
            this.btn_PGConsulta.Size = new System.Drawing.Size(149, 34);
            this.btn_PGConsulta.TabIndex = 110;
            this.btn_PGConsulta.Text = "Seleccionar";
            this.btn_PGConsulta.UseVisualStyleBackColor = false;
            this.btn_PGConsulta.Click += new System.EventHandler(this.btn_PGConsulta_Click);
            // 
            // cb_TipoComp
            // 
            this.cb_TipoComp.FormattingEnabled = true;
            this.cb_TipoComp.Location = new System.Drawing.Point(203, 104);
            this.cb_TipoComp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_TipoComp.Name = "cb_TipoComp";
            this.cb_TipoComp.Size = new System.Drawing.Size(199, 24);
            this.cb_TipoComp.TabIndex = 109;
            // 
            // comboBox9
            // 
            this.comboBox9.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox9.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Items.AddRange(new object[] {
            "Seleccione un Item...",
            "CC",
            "CE",
            "NI",
            "NP",
            "OT",
            "PA",
            "TI"});
            this.comboBox9.Location = new System.Drawing.Point(104, 180);
            this.comboBox9.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(179, 24);
            this.comboBox9.TabIndex = 108;
            this.comboBox9.Text = "Seleccione un Item...";
            // 
            // btn_PGGuardar
            // 
            this.btn_PGGuardar.BackColor = System.Drawing.Color.DarkGray;
            this.btn_PGGuardar.FlatAppearance.BorderSize = 0;
            this.btn_PGGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_PGGuardar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.btn_PGGuardar.Location = new System.Drawing.Point(311, 206);
            this.btn_PGGuardar.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_PGGuardar.Name = "btn_PGGuardar";
            this.btn_PGGuardar.Size = new System.Drawing.Size(149, 34);
            this.btn_PGGuardar.TabIndex = 107;
            this.btn_PGGuardar.Text = "Guardar";
            this.btn_PGGuardar.UseVisualStyleBackColor = false;
            this.btn_PGGuardar.Visible = false;
            this.btn_PGGuardar.Click += new System.EventHandler(this.btn_PGGuardar_Click);
            // 
            // dgv_ParametrosGenerales
            // 
            this.dgv_ParametrosGenerales.AllowUserToAddRows = false;
            this.dgv_ParametrosGenerales.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ParametrosGenerales.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.TipoComprobante,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.Empresa});
            this.dgv_ParametrosGenerales.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgv_ParametrosGenerales.Location = new System.Drawing.Point(3, 276);
            this.dgv_ParametrosGenerales.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv_ParametrosGenerales.Name = "dgv_ParametrosGenerales";
            this.dgv_ParametrosGenerales.RowTemplate.Height = 25;
            this.dgv_ParametrosGenerales.Size = new System.Drawing.Size(815, 330);
            this.dgv_ParametrosGenerales.TabIndex = 106;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Contabilidad";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 150;
            // 
            // TipoComprobante
            // 
            this.TipoComprobante.HeaderText = "Tipo de Comprobante";
            this.TipoComprobante.Name = "TipoComprobante";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Tipo ID";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 80;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "No ID";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 120;
            // 
            // Empresa
            // 
            this.Empresa.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Empresa.HeaderText = "Empresa";
            this.Empresa.Name = "Empresa";
            // 
            // comboBox8
            // 
            this.comboBox8.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.comboBox8.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.comboBox8.FormatString = "N0";
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(178, 77);
            this.comboBox8.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(225, 24);
            this.comboBox8.TabIndex = 105;
            this.comboBox8.SelectedIndexChanged += new System.EventHandler(this.comboBox8_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label7.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label7.Location = new System.Drawing.Point(15, 31);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(197, 24);
            this.label7.TabIndex = 17;
            this.label7.Text = "Parametros Generales";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label6.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(15, 145);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(197, 24);
            this.label6.TabIndex = 16;
            this.label6.Text = "Empresa que Factura ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(15, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(156, 18);
            this.label5.TabIndex = 14;
            this.label5.Text = "Tipo de Comprobante:";
            // 
            // txt_id
            // 
            this.txt_id.Location = new System.Drawing.Point(104, 207);
            this.txt_id.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txt_id.Name = "txt_id";
            this.txt_id.Size = new System.Drawing.Size(179, 22);
            this.txt_id.TabIndex = 11;
            // 
            // tp_ParametrosContables
            // 
            this.tp_ParametrosContables.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.tp_ParametrosContables.Controls.Add(this.lbl_IdCliente);
            this.tp_ParametrosContables.Controls.Add(this.btn_PCEditarCliente);
            this.tp_ParametrosContables.Controls.Add(this.btn_PCNombreCliente);
            this.tp_ParametrosContables.Controls.Add(this.textBox4);
            this.tp_ParametrosContables.Controls.Add(this.label22);
            this.tp_ParametrosContables.Controls.Add(this.lbl_PCNombreCliente);
            this.tp_ParametrosContables.Controls.Add(this.label10);
            this.tp_ParametrosContables.Controls.Add(this.cb_PCTipoID);
            this.tp_ParametrosContables.Controls.Add(this.dgv_ParametrosContables);
            this.tp_ParametrosContables.Controls.Add(this.btn_PCAgregarCliente);
            this.tp_ParametrosContables.Controls.Add(this.btn_PCBuscarCliente);
            this.tp_ParametrosContables.Controls.Add(this.label20);
            this.tp_ParametrosContables.Controls.Add(this.cb_PCContabilidad);
            this.tp_ParametrosContables.Controls.Add(this.label39);
            this.tp_ParametrosContables.Location = new System.Drawing.Point(4, 25);
            this.tp_ParametrosContables.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tp_ParametrosContables.Name = "tp_ParametrosContables";
            this.tp_ParametrosContables.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tp_ParametrosContables.Size = new System.Drawing.Size(821, 608);
            this.tp_ParametrosContables.TabIndex = 1;
            this.tp_ParametrosContables.Text = "Parametros Contables";
            // 
            // lbl_IdCliente
            // 
            this.lbl_IdCliente.AutoSize = true;
            this.lbl_IdCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.lbl_IdCliente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_IdCliente.Location = new System.Drawing.Point(33, 213);
            this.lbl_IdCliente.Name = "lbl_IdCliente";
            this.lbl_IdCliente.Size = new System.Drawing.Size(72, 16);
            this.lbl_IdCliente.TabIndex = 125;
            this.lbl_IdCliente.Text = "*Id Cliente*";
            this.lbl_IdCliente.Visible = false;
            // 
            // btn_PCEditarCliente
            // 
            this.btn_PCEditarCliente.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btn_PCEditarCliente.FlatAppearance.BorderSize = 0;
            this.btn_PCEditarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_PCEditarCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btn_PCEditarCliente.Location = new System.Drawing.Point(268, 78);
            this.btn_PCEditarCliente.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_PCEditarCliente.Name = "btn_PCEditarCliente";
            this.btn_PCEditarCliente.Size = new System.Drawing.Size(165, 57);
            this.btn_PCEditarCliente.TabIndex = 124;
            this.btn_PCEditarCliente.Text = "Editar Info Cliente";
            this.btn_PCEditarCliente.UseVisualStyleBackColor = false;
            this.btn_PCEditarCliente.Visible = false;
            this.btn_PCEditarCliente.Click += new System.EventHandler(this.btn_PCEditarCliente_Click);
            // 
            // btn_PCNombreCliente
            // 
            this.btn_PCNombreCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.btn_PCNombreCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.btn_PCNombreCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btn_PCNombreCliente.FormatString = "N0";
            this.btn_PCNombreCliente.FormattingEnabled = true;
            this.btn_PCNombreCliente.Location = new System.Drawing.Point(33, 177);
            this.btn_PCNombreCliente.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_PCNombreCliente.Name = "btn_PCNombreCliente";
            this.btn_PCNombreCliente.Size = new System.Drawing.Size(268, 24);
            this.btn_PCNombreCliente.TabIndex = 123;
            this.btn_PCNombreCliente.Text = "Seleccione Contabilidad...";
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.textBox4.Location = new System.Drawing.Point(35, 177);
            this.textBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBox4.MaxLength = 11;
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(227, 22);
            this.textBox4.TabIndex = 122;
            this.textBox4.Visible = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label22.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label22.Location = new System.Drawing.Point(31, 157);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(119, 16);
            this.label22.TabIndex = 121;
            this.label22.Text = "Nombre de Cliente";
            // 
            // lbl_PCNombreCliente
            // 
            this.lbl_PCNombreCliente.AutoSize = true;
            this.lbl_PCNombreCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_PCNombreCliente.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbl_PCNombreCliente.Location = new System.Drawing.Point(32, 229);
            this.lbl_PCNombreCliente.Name = "lbl_PCNombreCliente";
            this.lbl_PCNombreCliente.Size = new System.Drawing.Size(146, 20);
            this.lbl_PCNombreCliente.TabIndex = 118;
            this.lbl_PCNombreCliente.Text = "*Nombre Cliente*";
            this.lbl_PCNombreCliente.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label10.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label10.Location = new System.Drawing.Point(30, 110);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 16);
            this.label10.TabIndex = 114;
            this.label10.Text = "Tipo Id cliente";
            // 
            // cb_PCTipoID
            // 
            this.cb_PCTipoID.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_PCTipoID.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_PCTipoID.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.cb_PCTipoID.FormatString = "N0";
            this.cb_PCTipoID.FormattingEnabled = true;
            this.cb_PCTipoID.Items.AddRange(new object[] {
            "Seleccione un Item...",
            "CC",
            "CE",
            "NI",
            "NP",
            "OT",
            "PA",
            "TI"});
            this.cb_PCTipoID.Location = new System.Drawing.Point(32, 129);
            this.cb_PCTipoID.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_PCTipoID.Name = "cb_PCTipoID";
            this.cb_PCTipoID.Size = new System.Drawing.Size(229, 24);
            this.cb_PCTipoID.TabIndex = 115;
            this.cb_PCTipoID.Text = "Seleccione un Item...";
            this.cb_PCTipoID.SelectedIndexChanged += new System.EventHandler(this.cb_PCTipoID_SelectedIndexChanged);
            // 
            // dgv_ParametrosContables
            // 
            this.dgv_ParametrosContables.AllowUserToAddRows = false;
            this.dgv_ParametrosContables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_ParametrosContables.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.tipoIdCliente,
            this.NoIdCliente,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dgv_ParametrosContables.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgv_ParametrosContables.Location = new System.Drawing.Point(3, 276);
            this.dgv_ParametrosContables.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv_ParametrosContables.Name = "dgv_ParametrosContables";
            this.dgv_ParametrosContables.RowTemplate.Height = 25;
            this.dgv_ParametrosContables.Size = new System.Drawing.Size(815, 330);
            this.dgv_ParametrosContables.TabIndex = 113;
            // 
            // tipoIdCliente
            // 
            this.tipoIdCliente.HeaderText = "Tipo Id Cliente";
            this.tipoIdCliente.Name = "tipoIdCliente";
            // 
            // NoIdCliente
            // 
            this.NoIdCliente.HeaderText = "No Id Cliente";
            this.NoIdCliente.Name = "NoIdCliente";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Cod Cuenta";
            this.Column2.Name = "Column2";
            this.Column2.Width = 150;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column3.HeaderText = "Tipo Cuenta";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Tipo Mov";
            this.Column4.Items.AddRange(new object[] {
            "C",
            "D"});
            this.Column4.Name = "Column4";
            // 
            // btn_PCAgregarCliente
            // 
            this.btn_PCAgregarCliente.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btn_PCAgregarCliente.FlatAppearance.BorderSize = 0;
            this.btn_PCAgregarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_PCAgregarCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btn_PCAgregarCliente.Location = new System.Drawing.Point(598, 182);
            this.btn_PCAgregarCliente.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_PCAgregarCliente.Name = "btn_PCAgregarCliente";
            this.btn_PCAgregarCliente.Size = new System.Drawing.Size(195, 63);
            this.btn_PCAgregarCliente.TabIndex = 112;
            this.btn_PCAgregarCliente.Text = "Agregar Cliente";
            this.btn_PCAgregarCliente.UseVisualStyleBackColor = false;
            this.btn_PCAgregarCliente.Click += new System.EventHandler(this.btn_PCAgregarCliente_Click);
            // 
            // btn_PCBuscarCliente
            // 
            this.btn_PCBuscarCliente.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btn_PCBuscarCliente.FlatAppearance.BorderSize = 0;
            this.btn_PCBuscarCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_PCBuscarCliente.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.btn_PCBuscarCliente.Location = new System.Drawing.Point(307, 171);
            this.btn_PCBuscarCliente.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_PCBuscarCliente.Name = "btn_PCBuscarCliente";
            this.btn_PCBuscarCliente.Size = new System.Drawing.Size(133, 34);
            this.btn_PCBuscarCliente.TabIndex = 111;
            this.btn_PCBuscarCliente.Text = "Buscar Cliente";
            this.btn_PCBuscarCliente.UseVisualStyleBackColor = false;
            this.btn_PCBuscarCliente.Click += new System.EventHandler(this.btn_PCBuscarCliente_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label20.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label20.Location = new System.Drawing.Point(30, 62);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(89, 16);
            this.label20.TabIndex = 12;
            this.label20.Text = "Contabilidad :";
            // 
            // cb_PCContabilidad
            // 
            this.cb_PCContabilidad.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_PCContabilidad.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_PCContabilidad.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.cb_PCContabilidad.FormatString = "N0";
            this.cb_PCContabilidad.FormattingEnabled = true;
            this.cb_PCContabilidad.Location = new System.Drawing.Point(32, 81);
            this.cb_PCContabilidad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_PCContabilidad.Name = "cb_PCContabilidad";
            this.cb_PCContabilidad.Size = new System.Drawing.Size(230, 24);
            this.cb_PCContabilidad.TabIndex = 104;
            this.cb_PCContabilidad.Text = "Seleccione un Item...";
            this.cb_PCContabilidad.SelectedIndexChanged += new System.EventHandler(this.cb_PCContabilidad_SelectedIndexChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.label39.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label39.Location = new System.Drawing.Point(28, 18);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(194, 24);
            this.label39.TabIndex = 97;
            this.label39.Text = "Parametros Contables";
            // 
            // tp_CargarFactura
            // 
            this.tp_CargarFactura.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.tp_CargarFactura.Controls.Add(this.cb_CFTipoIdCliente);
            this.tp_CargarFactura.Controls.Add(this.label26);
            this.tp_CargarFactura.Controls.Add(this.tb_CFNoIdClente);
            this.tp_CargarFactura.Controls.Add(this.label25);
            this.tp_CargarFactura.Controls.Add(this.label19);
            this.tp_CargarFactura.Controls.Add(this.tb_CFFechaEmision);
            this.tp_CargarFactura.Controls.Add(this.label17);
            this.tp_CargarFactura.Controls.Add(this.label16);
            this.tp_CargarFactura.Controls.Add(this.tb_CFNoFactura);
            this.tp_CargarFactura.Controls.Add(this.label1);
            this.tp_CargarFactura.Controls.Add(this.Lbl_Abrir);
            this.tp_CargarFactura.Controls.Add(this.label8);
            this.tp_CargarFactura.Controls.Add(this.btn_CFBuscarDocumento);
            this.tp_CargarFactura.Controls.Add(this.label9);
            this.tp_CargarFactura.Controls.Add(this.cb_CFTipoId);
            this.tp_CargarFactura.Controls.Add(this.cb_CFContabiliad);
            this.tp_CargarFactura.Controls.Add(this.tb_CFNoId);
            this.tp_CargarFactura.Controls.Add(this.dgv_FacturaCargada);
            this.tp_CargarFactura.Controls.Add(this.label18);
            this.tp_CargarFactura.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tp_CargarFactura.Location = new System.Drawing.Point(4, 25);
            this.tp_CargarFactura.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tp_CargarFactura.Name = "tp_CargarFactura";
            this.tp_CargarFactura.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tp_CargarFactura.Size = new System.Drawing.Size(821, 608);
            this.tp_CargarFactura.TabIndex = 2;
            this.tp_CargarFactura.Text = "Cargar Factura XML";
            // 
            // cb_CFTipoIdCliente
            // 
            this.cb_CFTipoIdCliente.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_CFTipoIdCliente.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_CFTipoIdCliente.Enabled = false;
            this.cb_CFTipoIdCliente.FormattingEnabled = true;
            this.cb_CFTipoIdCliente.Items.AddRange(new object[] {
            "CC",
            "CE",
            "NI",
            "NP",
            "OT",
            "PA",
            "TI"});
            this.cb_CFTipoIdCliente.Location = new System.Drawing.Point(143, 224);
            this.cb_CFTipoIdCliente.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_CFTipoIdCliente.Name = "cb_CFTipoIdCliente";
            this.cb_CFTipoIdCliente.Size = new System.Drawing.Size(175, 24);
            this.cb_CFTipoIdCliente.TabIndex = 123;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label26.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label26.Location = new System.Drawing.Point(324, 205);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(89, 16);
            this.label26.TabIndex = 122;
            this.label26.Text = "No Id Cliente :";
            // 
            // tb_CFNoIdClente
            // 
            this.tb_CFNoIdClente.Enabled = false;
            this.tb_CFNoIdClente.Location = new System.Drawing.Point(324, 224);
            this.tb_CFNoIdClente.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_CFNoIdClente.Name = "tb_CFNoIdClente";
            this.tb_CFNoIdClente.Size = new System.Drawing.Size(169, 22);
            this.tb_CFNoIdClente.TabIndex = 121;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label25.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label25.Location = new System.Drawing.Point(143, 205);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(99, 16);
            this.label25.TabIndex = 120;
            this.label25.Text = "Tipo Id Cliente :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label19.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label19.Location = new System.Drawing.Point(16, 205);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(102, 16);
            this.label19.TabIndex = 118;
            this.label19.Text = "Fecha Emision :";
            // 
            // tb_CFFechaEmision
            // 
            this.tb_CFFechaEmision.Enabled = false;
            this.tb_CFFechaEmision.Location = new System.Drawing.Point(16, 224);
            this.tb_CFFechaEmision.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_CFFechaEmision.Name = "tb_CFFechaEmision";
            this.tb_CFFechaEmision.Size = new System.Drawing.Size(121, 22);
            this.tb_CFFechaEmision.TabIndex = 117;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label17.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label17.Location = new System.Drawing.Point(12, 175);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(179, 18);
            this.label17.TabIndex = 116;
            this.label17.Text = "Informacion de la Factura:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label16.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label16.Location = new System.Drawing.Point(499, 205);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(87, 16);
            this.label16.TabIndex = 115;
            this.label16.Text = "Factura XML :";
            // 
            // tb_CFNoFactura
            // 
            this.tb_CFNoFactura.Enabled = false;
            this.tb_CFNoFactura.Location = new System.Drawing.Point(499, 224);
            this.tb_CFNoFactura.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_CFNoFactura.Name = "tb_CFNoFactura";
            this.tb_CFNoFactura.Size = new System.Drawing.Size(169, 22);
            this.tb_CFNoFactura.TabIndex = 114;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(597, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 16);
            this.label1.TabIndex = 113;
            this.label1.Text = "No. ID Empresa que factura :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label8.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label8.Location = new System.Drawing.Point(316, 58);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(184, 16);
            this.label8.TabIndex = 112;
            this.label8.Text = "Tipo ID Empresa que factura :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label9.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label9.Location = new System.Drawing.Point(16, 58);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(89, 16);
            this.label9.TabIndex = 108;
            this.label9.Text = "Contabilidad :";
            // 
            // cb_CFTipoId
            // 
            this.cb_CFTipoId.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_CFTipoId.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_CFTipoId.Enabled = false;
            this.cb_CFTipoId.FormattingEnabled = true;
            this.cb_CFTipoId.Items.AddRange(new object[] {
            "CC",
            "CE",
            "NI",
            "NP",
            "OT",
            "PA",
            "TI"});
            this.cb_CFTipoId.Location = new System.Drawing.Point(327, 77);
            this.cb_CFTipoId.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_CFTipoId.Name = "cb_CFTipoId";
            this.cb_CFTipoId.Size = new System.Drawing.Size(156, 24);
            this.cb_CFTipoId.TabIndex = 111;
            // 
            // cb_CFContabiliad
            // 
            this.cb_CFContabiliad.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_CFContabiliad.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_CFContabiliad.FormatString = "N0";
            this.cb_CFContabiliad.FormattingEnabled = true;
            this.cb_CFContabiliad.Location = new System.Drawing.Point(17, 77);
            this.cb_CFContabiliad.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_CFContabiliad.Name = "cb_CFContabiliad";
            this.cb_CFContabiliad.Size = new System.Drawing.Size(201, 24);
            this.cb_CFContabiliad.TabIndex = 110;
            // 
            // tb_CFNoId
            // 
            this.tb_CFNoId.Enabled = false;
            this.tb_CFNoId.Location = new System.Drawing.Point(597, 77);
            this.tb_CFNoId.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_CFNoId.Name = "tb_CFNoId";
            this.tb_CFNoId.Size = new System.Drawing.Size(169, 22);
            this.tb_CFNoId.TabIndex = 109;
            // 
            // dgv_FacturaCargada
            // 
            this.dgv_FacturaCargada.AllowUserToAddRows = false;
            this.dgv_FacturaCargada.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_FacturaCargada.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.TipoId,
            this.Desription,
            this.Valor,
            this.Column5});
            this.dgv_FacturaCargada.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgv_FacturaCargada.Location = new System.Drawing.Point(3, 276);
            this.dgv_FacturaCargada.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv_FacturaCargada.Name = "dgv_FacturaCargada";
            this.dgv_FacturaCargada.RowTemplate.Height = 25;
            this.dgv_FacturaCargada.Size = new System.Drawing.Size(815, 330);
            this.dgv_FacturaCargada.TabIndex = 51;
            // 
            // TipoId
            // 
            this.TipoId.HeaderText = "Cod. Cuenta";
            this.TipoId.Name = "TipoId";
            this.TipoId.Width = 120;
            // 
            // Desription
            // 
            this.Desription.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Desription.HeaderText = "Descripcion";
            this.Desription.Name = "Desription";
            // 
            // Valor
            // 
            this.Valor.HeaderText = "Valor Mov";
            this.Valor.Name = "Valor";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Tipo Mov";
            this.Column5.Name = "Column5";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.label18.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label18.Location = new System.Drawing.Point(7, 18);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(229, 25);
            this.label18.TabIndex = 49;
            this.label18.Text = "Carga de Factura XML";
            // 
            // tp_FacturasCargadas
            // 
            this.tp_FacturasCargadas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(30)))), ((int)(((byte)(45)))));
            this.tp_FacturasCargadas.Controls.Add(this.label21);
            this.tp_FacturasCargadas.Controls.Add(this.btn_FCGenerarComprobante);
            this.tp_FacturasCargadas.Controls.Add(this.btn_FCGuardarExcel);
            this.tp_FacturasCargadas.Controls.Add(this.btn_FCFiltro);
            this.tp_FacturasCargadas.Controls.Add(this.label14);
            this.tp_FacturasCargadas.Controls.Add(this.cb_FCEmpresaFactura);
            this.tp_FacturasCargadas.Controls.Add(this.label13);
            this.tp_FacturasCargadas.Controls.Add(this.label11);
            this.tp_FacturasCargadas.Controls.Add(this.cb_FCEstado);
            this.tp_FacturasCargadas.Controls.Add(this.btn_FCVolver);
            this.tp_FacturasCargadas.Controls.Add(this.dgv_FCFacturasCargadas);
            this.tp_FacturasCargadas.Controls.Add(this.label12);
            this.tp_FacturasCargadas.Controls.Add(this.tb_FCIdFactura);
            this.tp_FacturasCargadas.Controls.Add(this.dataGridView1);
            this.tp_FacturasCargadas.Controls.Add(this.btn_FCConsultaFactura);
            this.tp_FacturasCargadas.Controls.Add(this.label15);
            this.tp_FacturasCargadas.Location = new System.Drawing.Point(4, 25);
            this.tp_FacturasCargadas.Margin = new System.Windows.Forms.Padding(0);
            this.tp_FacturasCargadas.Name = "tp_FacturasCargadas";
            this.tp_FacturasCargadas.Size = new System.Drawing.Size(821, 608);
            this.tp_FacturasCargadas.TabIndex = 3;
            this.tp_FacturasCargadas.Text = "Facturas Cargadas";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label21.Location = new System.Drawing.Point(102, 61);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(51, 16);
            this.label21.TabIndex = 133;
            this.label21.Text = "Fecha :";
            this.label21.Visible = false;
            // 
            // btn_FCGenerarComprobante
            // 
            this.btn_FCGenerarComprobante.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btn_FCGenerarComprobante.FlatAppearance.BorderSize = 0;
            this.btn_FCGenerarComprobante.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_FCGenerarComprobante.Location = new System.Drawing.Point(297, 49);
            this.btn_FCGenerarComprobante.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_FCGenerarComprobante.Name = "btn_FCGenerarComprobante";
            this.btn_FCGenerarComprobante.Size = new System.Drawing.Size(158, 36);
            this.btn_FCGenerarComprobante.TabIndex = 132;
            this.btn_FCGenerarComprobante.Text = "Generar Comprobante";
            this.btn_FCGenerarComprobante.UseVisualStyleBackColor = false;
            this.btn_FCGenerarComprobante.Visible = false;
            this.btn_FCGenerarComprobante.Click += new System.EventHandler(this.btn_FCGenerarComprobante_Click);
            // 
            // btn_FCGuardarExcel
            // 
            this.btn_FCGuardarExcel.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btn_FCGuardarExcel.FlatAppearance.BorderSize = 0;
            this.btn_FCGuardarExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_FCGuardarExcel.Location = new System.Drawing.Point(297, 89);
            this.btn_FCGuardarExcel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_FCGuardarExcel.Name = "btn_FCGuardarExcel";
            this.btn_FCGuardarExcel.Size = new System.Drawing.Size(158, 36);
            this.btn_FCGuardarExcel.TabIndex = 131;
            this.btn_FCGuardarExcel.Text = "Guardar en Excel";
            this.btn_FCGuardarExcel.UseVisualStyleBackColor = false;
            this.btn_FCGuardarExcel.Visible = false;
            this.btn_FCGuardarExcel.Click += new System.EventHandler(this.btn_FCGuardarExcel_Click);
            // 
            // btn_FCFiltro
            // 
            this.btn_FCFiltro.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btn_FCFiltro.FlatAppearance.BorderSize = 0;
            this.btn_FCFiltro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_FCFiltro.Location = new System.Drawing.Point(530, 89);
            this.btn_FCFiltro.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_FCFiltro.Name = "btn_FCFiltro";
            this.btn_FCFiltro.Size = new System.Drawing.Size(158, 36);
            this.btn_FCFiltro.TabIndex = 130;
            this.btn_FCFiltro.Text = "Aplicar Filtro";
            this.btn_FCFiltro.UseVisualStyleBackColor = false;
            this.btn_FCFiltro.Click += new System.EventHandler(this.btn_FCFiltro_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label14.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label14.Location = new System.Drawing.Point(462, 38);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(116, 16);
            this.label14.TabIndex = 129;
            this.label14.Text = "Empresa Factura :";
            // 
            // cb_FCEmpresaFactura
            // 
            this.cb_FCEmpresaFactura.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_FCEmpresaFactura.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_FCEmpresaFactura.FormatString = "N0";
            this.cb_FCEmpresaFactura.FormattingEnabled = true;
            this.cb_FCEmpresaFactura.Location = new System.Drawing.Point(465, 58);
            this.cb_FCEmpresaFactura.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_FCEmpresaFactura.Name = "cb_FCEmpresaFactura";
            this.cb_FCEmpresaFactura.Size = new System.Drawing.Size(156, 24);
            this.cb_FCEmpresaFactura.TabIndex = 128;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label13.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label13.Location = new System.Drawing.Point(626, 38);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 16);
            this.label13.TabIndex = 127;
            this.label13.Text = "Estado :";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.label11.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label11.Location = new System.Drawing.Point(595, 6);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(174, 25);
            this.label11.TabIndex = 126;
            this.label11.Text = "Filtros Busqueda";
            // 
            // cb_FCEstado
            // 
            this.cb_FCEstado.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.cb_FCEstado.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cb_FCEstado.FormatString = "N0";
            this.cb_FCEstado.FormattingEnabled = true;
            this.cb_FCEstado.Items.AddRange(new object[] {
            "Seleccione un Item...",
            "Cargado",
            "Exportado"});
            this.cb_FCEstado.Location = new System.Drawing.Point(627, 58);
            this.cb_FCEstado.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cb_FCEstado.Name = "cb_FCEstado";
            this.cb_FCEstado.Size = new System.Drawing.Size(153, 24);
            this.cb_FCEstado.TabIndex = 125;
            this.cb_FCEstado.Text = "Seleccione un Item...";
            // 
            // btn_FCVolver
            // 
            this.btn_FCVolver.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btn_FCVolver.FlatAppearance.BorderSize = 0;
            this.btn_FCVolver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_FCVolver.Location = new System.Drawing.Point(188, 94);
            this.btn_FCVolver.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_FCVolver.Name = "btn_FCVolver";
            this.btn_FCVolver.Size = new System.Drawing.Size(87, 36);
            this.btn_FCVolver.TabIndex = 124;
            this.btn_FCVolver.Text = "Volver";
            this.btn_FCVolver.UseVisualStyleBackColor = false;
            this.btn_FCVolver.Visible = false;
            this.btn_FCVolver.Click += new System.EventHandler(this.btn_FCVolver_Click);
            // 
            // dgv_FCFacturasCargadas
            // 
            this.dgv_FCFacturasCargadas.AllowUserToAddRows = false;
            this.dgv_FCFacturasCargadas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_FCFacturasCargadas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FC_Grid_Contabilidad,
            this.FC_Grid_NoIdEmpFactura,
            this.FC_Grid_NoIdCliente,
            this.FC_Grid_FechaEmision,
            this.FC_Grid_VContable,
            this.FC_Grid_Valor,
            this.FC_Grid_TMov});
            this.dgv_FCFacturasCargadas.Location = new System.Drawing.Point(0, 180);
            this.dgv_FCFacturasCargadas.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgv_FCFacturasCargadas.MultiSelect = false;
            this.dgv_FCFacturasCargadas.Name = "dgv_FCFacturasCargadas";
            this.dgv_FCFacturasCargadas.ReadOnly = true;
            this.dgv_FCFacturasCargadas.RowTemplate.Height = 25;
            this.dgv_FCFacturasCargadas.Size = new System.Drawing.Size(830, 423);
            this.dgv_FCFacturasCargadas.TabIndex = 123;
            this.dgv_FCFacturasCargadas.Visible = false;
            // 
            // FC_Grid_Contabilidad
            // 
            this.FC_Grid_Contabilidad.HeaderText = "Contabilidad";
            this.FC_Grid_Contabilidad.Name = "FC_Grid_Contabilidad";
            this.FC_Grid_Contabilidad.ReadOnly = true;
            this.FC_Grid_Contabilidad.Width = 120;
            // 
            // FC_Grid_NoIdEmpFactura
            // 
            this.FC_Grid_NoIdEmpFactura.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.FC_Grid_NoIdEmpFactura.HeaderText = "Id Empresa Factura";
            this.FC_Grid_NoIdEmpFactura.Name = "FC_Grid_NoIdEmpFactura";
            this.FC_Grid_NoIdEmpFactura.ReadOnly = true;
            // 
            // FC_Grid_NoIdCliente
            // 
            this.FC_Grid_NoIdCliente.HeaderText = "Id Cliente";
            this.FC_Grid_NoIdCliente.Name = "FC_Grid_NoIdCliente";
            this.FC_Grid_NoIdCliente.ReadOnly = true;
            // 
            // FC_Grid_FechaEmision
            // 
            this.FC_Grid_FechaEmision.HeaderText = "Cod Cuenta";
            this.FC_Grid_FechaEmision.Name = "FC_Grid_FechaEmision";
            this.FC_Grid_FechaEmision.ReadOnly = true;
            // 
            // FC_Grid_VContable
            // 
            this.FC_Grid_VContable.HeaderText = "Cuenta";
            this.FC_Grid_VContable.Name = "FC_Grid_VContable";
            this.FC_Grid_VContable.ReadOnly = true;
            // 
            // FC_Grid_Valor
            // 
            this.FC_Grid_Valor.HeaderText = "Valor";
            this.FC_Grid_Valor.Name = "FC_Grid_Valor";
            this.FC_Grid_Valor.ReadOnly = true;
            // 
            // FC_Grid_TMov
            // 
            this.FC_Grid_TMov.FillWeight = 80F;
            this.FC_Grid_TMov.HeaderText = "Tipo Mov";
            this.FC_Grid_TMov.Items.AddRange(new object[] {
            "C",
            "D"});
            this.FC_Grid_TMov.Name = "FC_Grid_TMov";
            this.FC_Grid_TMov.ReadOnly = true;
            this.FC_Grid_TMov.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FC_Grid_TMov.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.FC_Grid_TMov.Width = 80;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            this.label12.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label12.Location = new System.Drawing.Point(21, 43);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(74, 16);
            this.label12.TabIndex = 120;
            this.label12.Text = "ID Factura :";
            // 
            // tb_FCIdFactura
            // 
            this.tb_FCIdFactura.Location = new System.Drawing.Point(23, 63);
            this.tb_FCIdFactura.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tb_FCIdFactura.Name = "tb_FCIdFactura";
            this.tb_FCIdFactura.Size = new System.Drawing.Size(76, 22);
            this.tb_FCIdFactura.TabIndex = 116;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.Column6,
            this.Column7,
            this.FC_Grid_Estado});
            this.dataGridView1.Location = new System.Drawing.Point(4, 180);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowTemplate.Height = 25;
            this.dataGridView1.Size = new System.Drawing.Size(830, 423);
            this.dataGridView1.TabIndex = 113;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "Id Factura";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 85;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Contabilidad";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewTextBoxColumn3.HeaderText = "Empresa que factura";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column6.HeaderText = "Cliente";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.HeaderText = "Fecha";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // FC_Grid_Estado
            // 
            this.FC_Grid_Estado.HeaderText = "Estado";
            this.FC_Grid_Estado.Name = "FC_Grid_Estado";
            this.FC_Grid_Estado.ReadOnly = true;
            // 
            // btn_FCConsultaFactura
            // 
            this.btn_FCConsultaFactura.BackColor = System.Drawing.SystemColors.ControlDark;
            this.btn_FCConsultaFactura.FlatAppearance.BorderSize = 0;
            this.btn_FCConsultaFactura.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_FCConsultaFactura.Location = new System.Drawing.Point(24, 94);
            this.btn_FCConsultaFactura.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btn_FCConsultaFactura.Name = "btn_FCConsultaFactura";
            this.btn_FCConsultaFactura.Size = new System.Drawing.Size(158, 36);
            this.btn_FCConsultaFactura.TabIndex = 114;
            this.btn_FCConsultaFactura.Text = "Cosultar Factura";
            this.btn_FCConsultaFactura.UseVisualStyleBackColor = false;
            this.btn_FCConsultaFactura.Click += new System.EventHandler(this.btn_FCConsultaFactura_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F);
            this.label15.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label15.Location = new System.Drawing.Point(20, 11);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(191, 25);
            this.label15.TabIndex = 112;
            this.label15.Text = "Facturas cargadas";
            // 
            // Contabilidad
            // 
            this.Contabilidad.HeaderText = "Contabilidad";
            this.Contabilidad.Name = "Contabilidad";
            // 
            // btn_MenuParametrosGenerales
            // 
            this.btn_MenuParametrosGenerales.FlatAppearance.BorderSize = 0;
            this.btn_MenuParametrosGenerales.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_MenuParametrosGenerales.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_MenuParametrosGenerales.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_MenuParametrosGenerales.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_MenuParametrosGenerales.Location = new System.Drawing.Point(3, 137);
            this.btn_MenuParametrosGenerales.Name = "btn_MenuParametrosGenerales";
            this.btn_MenuParametrosGenerales.Size = new System.Drawing.Size(203, 55);
            this.btn_MenuParametrosGenerales.TabIndex = 115;
            this.btn_MenuParametrosGenerales.Text = "Parametros Generales";
            this.btn_MenuParametrosGenerales.UseVisualStyleBackColor = true;
            this.btn_MenuParametrosGenerales.Click += new System.EventHandler(this.btn_MenuParametrosGenerales_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(11)))), ((int)(((byte)(7)))), ((int)(((byte)(17)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.btn_MenuSalir);
            this.panel1.Controls.Add(this.btn_MenuFacturasCargadas);
            this.panel1.Controls.Add(this.btn_MenuCargarFactura);
            this.panel1.Controls.Add(this.btn_MenuParametrosContables);
            this.panel1.Controls.Add(this.btn_MenuParametrosGenerales);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(209, 611);
            this.panel1.TabIndex = 12;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.InitialImage")));
            this.pictureBox1.Location = new System.Drawing.Point(2, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(204, 81);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 120;
            this.pictureBox1.TabStop = false;
            // 
            // btn_MenuSalir
            // 
            this.btn_MenuSalir.FlatAppearance.BorderSize = 0;
            this.btn_MenuSalir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_MenuSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_MenuSalir.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_MenuSalir.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_MenuSalir.Location = new System.Drawing.Point(2, 548);
            this.btn_MenuSalir.Name = "btn_MenuSalir";
            this.btn_MenuSalir.Size = new System.Drawing.Size(204, 55);
            this.btn_MenuSalir.TabIndex = 119;
            this.btn_MenuSalir.Text = "Salir";
            this.btn_MenuSalir.UseVisualStyleBackColor = true;
            this.btn_MenuSalir.Click += new System.EventHandler(this.button14_Click);
            // 
            // btn_MenuFacturasCargadas
            // 
            this.btn_MenuFacturasCargadas.FlatAppearance.BorderSize = 0;
            this.btn_MenuFacturasCargadas.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_MenuFacturasCargadas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_MenuFacturasCargadas.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_MenuFacturasCargadas.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_MenuFacturasCargadas.Location = new System.Drawing.Point(2, 320);
            this.btn_MenuFacturasCargadas.Name = "btn_MenuFacturasCargadas";
            this.btn_MenuFacturasCargadas.Size = new System.Drawing.Size(204, 55);
            this.btn_MenuFacturasCargadas.TabIndex = 118;
            this.btn_MenuFacturasCargadas.Text = "Facturas Cargadas";
            this.btn_MenuFacturasCargadas.UseVisualStyleBackColor = true;
            this.btn_MenuFacturasCargadas.Click += new System.EventHandler(this.mn_Facturas_Cargadas_Click);
            // 
            // btn_MenuCargarFactura
            // 
            this.btn_MenuCargarFactura.FlatAppearance.BorderSize = 0;
            this.btn_MenuCargarFactura.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_MenuCargarFactura.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_MenuCargarFactura.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_MenuCargarFactura.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_MenuCargarFactura.Location = new System.Drawing.Point(2, 259);
            this.btn_MenuCargarFactura.Name = "btn_MenuCargarFactura";
            this.btn_MenuCargarFactura.Size = new System.Drawing.Size(204, 55);
            this.btn_MenuCargarFactura.TabIndex = 117;
            this.btn_MenuCargarFactura.Text = "Cargar Factura";
            this.btn_MenuCargarFactura.UseVisualStyleBackColor = true;
            this.btn_MenuCargarFactura.Click += new System.EventHandler(this.mn_Cargar_Factura_Click);
            // 
            // btn_MenuParametrosContables
            // 
            this.btn_MenuParametrosContables.FlatAppearance.BorderSize = 0;
            this.btn_MenuParametrosContables.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.btn_MenuParametrosContables.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_MenuParametrosContables.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_MenuParametrosContables.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_MenuParametrosContables.Location = new System.Drawing.Point(2, 198);
            this.btn_MenuParametrosContables.Name = "btn_MenuParametrosContables";
            this.btn_MenuParametrosContables.Size = new System.Drawing.Size(204, 55);
            this.btn_MenuParametrosContables.TabIndex = 116;
            this.btn_MenuParametrosContables.Text = "Parametros Contables";
            this.btn_MenuParametrosContables.UseVisualStyleBackColor = true;
            this.btn_MenuParametrosContables.Click += new System.EventHandler(this.btn_MenuParametrosContables_Click);
            // 
            // Pagina_Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(23)))), ((int)(((byte)(21)))), ((int)(((byte)(32)))));
            this.ClientSize = new System.Drawing.Size(1034, 611);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1050, 750);
            this.MinimumSize = new System.Drawing.Size(150, 650);
            this.Name = "Pagina_Principal";
            this.Text = "Facturacion";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tp_ParametrosGenerales.ResumeLayout(false);
            this.tp_ParametrosGenerales.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ParametrosGenerales)).EndInit();
            this.tp_ParametrosContables.ResumeLayout(false);
            this.tp_ParametrosContables.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_ParametrosContables)).EndInit();
            this.tp_CargarFactura.ResumeLayout(false);
            this.tp_CargarFactura.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_FacturaCargada)).EndInit();
            this.tp_FacturasCargadas.ResumeLayout(false);
            this.tp_FacturasCargadas.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_FCFacturasCargadas)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_CFBuscarDocumento;
        private System.Windows.Forms.Label Lbl_Abrir;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tp_ParametrosGenerales;
        private System.Windows.Forms.TextBox txt_id;
        private System.Windows.Forms.TabPage tp_CargarFactura;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox tb_FCIdFactura;
        private System.Windows.Forms.TabPage tp_FacturasCargadas;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dgv_FacturaCargada;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cb_CFTipoId;
        private System.Windows.Forms.ComboBox cb_CFContabiliad;
        private System.Windows.Forms.TextBox tb_CFNoId;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btn_FCConsultaFactura;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.Button btn_PGGuardar;
        private System.Windows.Forms.DataGridView dgv_ParametrosGenerales;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Contabilidad;
        private System.Windows.Forms.ComboBox cb_TipoComp;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tb_CFNoFactura;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoComprobante;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Empresa;
        private System.Windows.Forms.TabPage tp_ParametrosContables;
        private System.Windows.Forms.Button btn_PCBuscarCliente;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cb_PCContabilidad;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox tb_CFNoIdClente;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox tb_CFFechaEmision;
        private System.Windows.Forms.ComboBox cb_CFTipoIdCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoId;
        private System.Windows.Forms.DataGridViewTextBoxColumn Desription;
        private System.Windows.Forms.DataGridViewTextBoxColumn Valor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.Button btn_PCAgregarCliente;
        private System.Windows.Forms.DataGridView dgv_FCFacturasCargadas;
        private System.Windows.Forms.Button btn_FCVolver;
        private System.Windows.Forms.DataGridView dgv_ParametrosContables;
        private System.Windows.Forms.Button btn_FCFiltro;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cb_FCEmpresaFactura;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cb_FCEstado;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn FC_Grid_Estado;
        private System.Windows.Forms.Button btn_FCGuardarExcel;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btn_PGConsulta;
        private System.Windows.Forms.DataGridViewTextBoxColumn tipoIdCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn NoIdCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewComboBoxColumn Column4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cb_PCTipoID;
        private System.Windows.Forms.Label lbl_PCNombreCliente;
        private System.Windows.Forms.Button btn_PGAgregar;
        private System.Windows.Forms.Button btn_PGActualizar;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button btn_PGVolver;
        private System.Windows.Forms.ComboBox btn_PCNombreCliente;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btn_PCEditarCliente;
        private System.Windows.Forms.Label lbl_IdCliente;
        private System.Windows.Forms.Button btn_FCGenerarComprobante;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.DataGridViewTextBoxColumn FC_Grid_Contabilidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn FC_Grid_NoIdEmpFactura;
        private System.Windows.Forms.DataGridViewTextBoxColumn FC_Grid_NoIdCliente;
        private System.Windows.Forms.DataGridViewTextBoxColumn FC_Grid_FechaEmision;
        private System.Windows.Forms.DataGridViewTextBoxColumn FC_Grid_VContable;
        private System.Windows.Forms.DataGridViewTextBoxColumn FC_Grid_Valor;
        private System.Windows.Forms.DataGridViewComboBoxColumn FC_Grid_TMov;
        private System.Windows.Forms.Button btn_MenuParametrosGenerales;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_MenuSalir;
        private System.Windows.Forms.Button btn_MenuFacturasCargadas;
        private System.Windows.Forms.Button btn_MenuCargarFactura;
        private System.Windows.Forms.Button btn_MenuParametrosContables;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

